package com.bancolombia.contratacion.steps;

import com.bancolombia.contratacion.page.IngresoNinjaStorePage;
import net.thucydides.core.annotations.Step;

public class IngresoNinjaStoreSteps {
	
	IngresoNinjaStorePage ingresoNinjaStorePage;
	
	@Step
	public void AbrirAplicacion() {
		ingresoNinjaStorePage.open();		
	}
	
	@Step
	public void IngresoMyAccount() {
		ingresoNinjaStorePage.MyAccount();
		
	}
	
	@Step
	public void IngresoLogin() {
		ingresoNinjaStorePage.Login();
		
	}
	
	@Step
	public void IngresoRegistrar() {
		ingresoNinjaStorePage.Registrar();
		
	}
	
}
